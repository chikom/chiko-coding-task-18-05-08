﻿using System.IO;
using System.Web;

namespace NumberGenerator.Data
{
    public static class FilePath
    {
        public static string AppData => Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "App_Data");
    }
}
