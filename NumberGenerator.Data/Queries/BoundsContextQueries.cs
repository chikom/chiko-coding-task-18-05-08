﻿using System.IO;
using static System.IO.File;
using static NumberGenerator.Data.FilePath;

namespace NumberGenerator.Data.Queries
{
    public class BoundsContextQueries
    {
        /// <summary>
        /// Gets the lower and upper bounds saved in the database
        /// </summary>
        /// <returns></returns>
        public static string GetBounds()
        {
            try
            {
                return ReadAllText(Path.Combine(AppData, "bounds.json"));
            }
            catch
            {
                return string.Empty;
            }            
        }
    }
}
