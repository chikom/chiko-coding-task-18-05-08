﻿using System.IO;
using static NumberGenerator.Data.FilePath;
using static System.IO.File;

namespace NumberGenerator.Data.Commands
{
    public static class BoundsContextCommands
    {
        /// <summary>
        /// Updates the lower and upper bounds in the database
        /// </summary>
        /// <param name="bounds"></param>
        public static void UpdateBounds(string bounds)
        {
            try
            {
                WriteAllText(Path.Combine(AppData, "bounds.json"), bounds);
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// Deletes the lower and upper bounds in the database
        /// </summary>        
        public static void DeleteBounds()
        {
            try
            {
                var emptyObject = "{}";
                WriteAllText(Path.Combine(AppData, "bounds.json"), emptyObject);
            }
            catch
            {
                return;
            }
        }
    }
}
