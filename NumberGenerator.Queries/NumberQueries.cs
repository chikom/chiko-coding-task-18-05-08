﻿using System.Collections.Generic;
using static NumberGenerator.Library.Data.FormattingRules;
using static NumberGenerator.Library.NumberGenerator;
using static NumberGenerator.Library.NumberFormatter;
using static NumberGenerator.Queries.BoundsQueries;

namespace NumberGenerator.Queries
{
    public static class NumberQueries
    {
        /// <summary>
        /// Gets a list of formatted numbers within the given upper and lowr bounds
        /// </summary>        
        /// <returns></returns>
        public static IEnumerable<string> GetFormattedNumbers() =>
            FormatNumbers(GenerateNumbers(GetBounds()), GetFormattingRules());
    }
}
