﻿using NumberGenerator.Data.Queries;
using NumberGenerator.Library;
using static Newtonsoft.Json.JsonConvert;
namespace NumberGenerator.Queries
{
    public static class BoundsQueries
    {
        /// <summary>
        /// Gets the lower and upper bounds saved in the database
        /// </summary>
        /// <returns></returns>
        public static Bounds GetBounds() =>
            DeserializeObject<Bounds>(BoundsContextQueries.GetBounds());

    }
}
