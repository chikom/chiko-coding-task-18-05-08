﻿using NumberGenerator.Data.Commands;
using NumberGenerator.Library;
using static Newtonsoft.Json.JsonConvert;

namespace NumberGenerator.Commands
{
    public static class BoundsCommands
    {
        /// <summary>
        /// Updates the upper and lower bounds in the database
        /// </summary>
        /// <param name="bounds"></param>
        public static void UpdateBounds(Bounds bounds)
        {
            BoundsContextCommands.UpdateBounds(SerializeObject(bounds));
        }

        /// <summary>
        /// Deletes the upper and lower bounds in the database
        /// </summary>        
        public static void DeleteBounds()
        {
            BoundsContextCommands.DeleteBounds();
        }
    }
}
