﻿using NumberGenerator.Library;
using System.Web.Http;
using static NumberGenerator.Queries.BoundsQueries;

namespace NumberGenerator.Controllers
{
    public class BoundsController : ApiController
    {
        // GET api/numbers
        /// <summary>
        /// Gets a list of formatted numbers within the given upper and lowr bounds
        /// </summary>
        /// <returns></returns>
        public Bounds Get()
        {
            return GetBounds();
        }
    }
}
