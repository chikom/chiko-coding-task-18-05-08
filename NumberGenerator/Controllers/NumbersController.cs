﻿using NumberGenerator.Library;
using System.Collections.Generic;
using System.Web.Http;
using static NumberGenerator.Commands.BoundsCommands;
using static NumberGenerator.Queries.NumberQueries;

namespace NumberGenerator.Controllers
{
    public class NumbersController : ApiController
    {
        // GET api/numbers
        /// <summary>
        /// Gets a list of formatted numbers within the given upper and lowr bounds
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> Get()
        {
            return GetFormattedNumbers();
        }

        // POST api/numbers
        public void Post([FromBody]Bounds bounds)
        {
            UpdateBounds(bounds);
        }

        // DELETE api/numbers
        public void Delete()
        {
            DeleteBounds();
        }
    }
}
