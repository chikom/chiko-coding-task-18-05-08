﻿/**
 * Creates an element
 * @param {NewElementArgument} newElementProperties
 */
function customCreateElement(newElementProperties) {
    if (!(newElementProperties && newElementProperties.tagName)) {
        console.log("Could not create element");
        return null;
    }
    var element = document.createElement(newElementProperties.tagName);

    setText(element, newElementProperties.text);
    addClasses(element, newElementProperties.classes);
    setAttributes(element, newElementProperties.attributes);
    setMinimizedAttributes(element, newElementProperties.minimizedAttributes);
    appendChildren(element, newElementProperties.children);

    return element;
}

/**
 * Adds classes to an element
 * @param {HTMLElement} element
 * @param {Array<string>} classes
 */
function addClasses(element, classes) {
    if (!(element && classes)) {
        return;
    }

    classes.forEach(function (aClass) {
        element.classList.add(aClass);
    });
}

/**
 * Sets the attributes of an element
 * @param {HTMLElement} element
 * @param {Array<Attribute>} attributes
 */
function setAttributes(element, attributes) {
    if (!(element && attributes)) {
        return;
    }

    attributes.forEach(function (attribute) {
        if (!(attribute.name && attribute.value)) {
            return;
        }

        element.setAttribute(attribute.name, attribute.value);
    });
}

/**
 * Sets the text of an element
 * @param {HTMLElement} element
 * @param {string} text
 */
function setText(element, text) {
    if (!(element && text)) {
        return;
    }

    element.appendChild(document.createTextNode(text));
}

/**
 * Sets the minimised attributes of an element e.g. element.hidden = true
 * @param {HTMLElement} element
 * @param {Array<Attribute>} attributes
 */
function setMinimizedAttributes(element, attributes) {
    if (!(element && attributes)) {
        return;
    }

    attributes.forEach(function (attribute) {
        if (!(attribute.name && attribute.value)) {
            return;
        }

        element[attribute.name] = attribute.value;
    });
}

/**
 * Appends children to a parent element
 * @param {HTMLElement} element
 * @param {Array<NewElementArgument>} children
 */
function appendChildren(element, children) {
    if (!(element && children)) {
        return;
    }

    children.forEach(function (child) {
        element.appendChild(customCreateElement(child));
    });
}