﻿/**
 * Represents the argument to pass when creating a new element
 */
class NewElementArgument {
    /**     
     * @param {string} tagName
     * @param {string} text
     * @param {Array<string>} classes
     * @param {Array<SimpleAttribute>} attributes
     * @param {Array<SimpleAttribute>} minimizedAttributes
     * @param {Array<NewElementArgument>} children
     */
    constructor(tagName, text, classes, attributes, minimizedAttributes, children) {
        this.tagName = tagName;
        this.text = text;
        this.classes = classes;
        this.attributes = attributes;
        this.minimizedAttributes = minimizedAttributes;
        this.children = children;
    }
}