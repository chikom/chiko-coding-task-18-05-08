﻿(function () {
    addListViewEventListeners();
    addRowViewEventListeners();
})();

function addListViewEventListeners() {
    var viewListButton = document.getElementById("view-list");
    if (!viewListButton) {
        return;
    }

    viewListButton.addEventListener("click", function () {
        viewListButton.hidden = true;
        document.getElementById("view-rows").hidden = false;
        document.getElementById("numbers").classList.add("rows");
    })
}

function addRowViewEventListeners() {
    var viewRowsButton = document.getElementById("view-rows");
    if (!viewRowsButton) {
        return;
    }

    viewRowsButton.addEventListener("click", function () {
        viewRowsButton.hidden = true;
        document.getElementById("view-list").hidden = false;
        document.getElementById("numbers").classList.remove("rows");
    })
}