﻿(function () {
    addBoundsInputEventListeners("Lower");
    addBoundsInputEventListeners("Upper");
    addDeleteBoundsButtonEventListeners();
})();

/**
 * Add lower bound input event listeners
 */
function addBoundsInputEventListeners(upperOrLower) {
    var upperOrLowerBoundText = upperOrLower + "Bound";
    var upperOrLowerBoundInput = document.getElementById(upperOrLowerBoundText);
    if (!upperOrLowerBoundInput) {
        return;
    }

    upperOrLowerBoundInput.addEventListener("input", function (event) {
        updateBound(event, upperOrLowerBoundText)
    });
}

/**
 * Update lower bound and refresh displayed numbers
 * @param {any} event
 * @param {string} upperOrLowerBoundText
 */
function updateBound(event, upperOrLowerBoundText) {    
    var bounds = {};
    $.get(ApiConstants.BOUNDS_URL, function (savedBounds) {
        bounds = savedBounds;
    })
    .done(function () {
        bounds[upperOrLowerBoundText] = event.target.value;

        $.post(ApiConstants.NUMBERS_URL, bounds)
            .done(printNumbersFromServer);
    });
}


/**
 * Adds event listener to the delete bounds button
 */
function addDeleteBoundsButtonEventListeners() {
    var refreshBoundsButton = document.getElementById("refresh-bounds");
    if (!refreshBoundsButton) {
        return;
    }

    refreshBoundsButton.addEventListener("click", refreshBounds);    
}

/**
 * Refreshes the lower and upper bounds to their default values and updates the displayed numbers
 */
function refreshBounds() {
    $.ajax({
        type: "DELETE",
        url: ApiConstants.NUMBERS_URL,
        success: printNumbersFromServer
    });
}