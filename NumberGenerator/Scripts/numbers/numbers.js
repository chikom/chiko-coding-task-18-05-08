﻿(function () {
    printNumbersFromServer();   
})();

/**
 * Prints the list of numbers
 * @param {Array<number>} numbers
 */
function printNumbers(numbers) {
    var numbersDiv = document.getElementById("numbers");
    if (!numbersDiv) {
        return;
    }
    numbersDiv.innerHTML = "";

    numbers.forEach(function (currentNumber) {                
        numbersDiv.appendChild(customCreateElement({ tagName: "div", text: currentNumber, classes: [currentNumber] }));
    });
}

/**
 * Prints the numbers retrieved from the server
 */
function printNumbersFromServer() {
    $.get(ApiConstants.NUMBERS_URL, printNumbers); 
}