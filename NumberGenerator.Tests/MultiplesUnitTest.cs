﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using static NumberGenerator.Library.Multiples;

namespace NumberGenerator.Tests
{
    [TestClass]
    public class MultiplesUnitTest
    {
        /// <summary>
        /// Passes if a multiple is correctly identified
        /// </summary>
        [TestMethod]
        [TestCategory("Multiples tests")]
        public void ShouldReturnTrueIfMultipleOfANumberIsPassed()
        {
            //arrange
            var multipleFactorPairs =
            new[] {
                new { multiple = 14, factor = 2 },
                new { multiple = 15, factor = 3 },
                new { multiple = 15, factor = 5 },
                new { multiple = 14, factor = 7 },
                new { multiple = 33, factor = 11 }
            };

            //act
            var correctlyIdentifiedAMultiple = false;
            
            foreach (var mfp in multipleFactorPairs)
            {
                correctlyIdentifiedAMultiple = mfp.multiple.IsAMultipleOf(mfp.factor);
                if (!correctlyIdentifiedAMultiple)
                {
                    Assert.Fail("Should have returned true when a multiple of a number was passed");
                    return;
                }
            }                              
        }

        /// <summary>
        /// Fails if a non-multiple is incorrectly identified as a multiple
        /// </summary>
        [TestMethod]
        [TestCategory("Multiples tests")]
        public void ShouldReturnTrueIfANonMultipleOfANumberIsPassed()
        {
            //arrange
            var multiple = 15;
            var factor = 2;
            
            //act
            //assert
            Assert.IsFalse(multiple.IsAMultipleOf(factor), "Should have returned false when a non-multiple of a number was passed");                           
        }
    }
}
