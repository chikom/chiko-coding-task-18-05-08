﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberGenerator.Library;
using System.Collections.Generic;
using static NumberGenerator.Library.Data.TextOutputConstants;
using static NumberGenerator.Library.NumberFormatter;

namespace NumberGenerator.Tests
{
    [TestClass]
    public class NumberFormatterUnitTest
    {
        
        /// <summary>
        /// Passes when the word 'three' is returned when a multiple of 3 is passed
        /// </summary>
        [TestMethod]
        [TestCategory("NumberFormatter tests")]
        public void ShouldReturnTheWordThreeWhenNumberIsAMultipleOfThree()
        {
            //arrange
            var number = 12;

            //act
            var formattedNumber = FormatConditionally(number, _FormattingRules);

            //assert
            Assert.IsTrue(formattedNumber == "Three", "Should have returned the word 'three' when a multiple of 3 was passed");
        }

        /// <summary>
        /// Passes when the word 'five' is returned when a multiple of 5 is passed
        /// </summary>
        [TestMethod]
        [TestCategory("NumberFormatter tests")]
        public void ShouldReturnTheWordFiveWhenNumberIsAMultipleOfFive()
        {
            //arrange
            var number = 25;

            //act
            var formattedNumber = FormatConditionally(number, _FormattingRules);

            //assert
            Assert.IsTrue(formattedNumber == "Five", "Should have returned the word 'five' when a multiple of 5 was passed");
        }

        /// <summary>
        /// Passes when the word 'Eurofins' is returned when a multiple of 3 and 5 is passed
        /// </summary>
        [TestMethod]
        [TestCategory("NumberFormatter tests")]
        public void ShouldReturnTheWordEurofinsWhenNumberIsAMultipleOfThreeAndFive()
        {
            //arrange
            var number = 15;

            //act
            var formattedNumber = FormatConditionally(number, _FormattingRules);

            //assert
            Assert.IsTrue(formattedNumber == "Eurofins", "Should have returned the word 'Eurofins' when a multiple of 3 and 5 was passed"); ;
        }

        /// <summary>
        /// Passes when a non-multiple of 3 and 5 is returned as a number
        /// </summary>
        [TestMethod]
        [TestCategory("NumberFormatter tests")]
        public void ShouldReturnNumberAsStringIfNumberNotAMultipleOfThreeOrFive()
        {
            //arrange
            var number = 16;

            //act
            var formattedNumber = FormatConditionally(number, _FormattingRules);

            //assert
            Assert.IsTrue(formattedNumber == 16.ToString(), "Should have returned the number as a string when a number that is not a multiple of 3 or 5 was passed");
        }

        private Dictionary<string, FormattingRule<int>> _FormattingRules = new Dictionary<string, FormattingRule<int>>
            {
                {
                    THREE,
                    new FormattingRule<int>
                    {
                        Compatible = number => number.IsAMultipleOf(3) && !number.IsAMultipleOf(5),
                        Format = number => THREE
                    }
                },
                {
                    FIVE,
                    new FormattingRule<int>
                    {
                        Compatible = number => number.IsAMultipleOf(5) && !number.IsAMultipleOf(3),
                        Format = number => FIVE
                    }
                },
                {
                    EUROFINS,
                    new FormattingRule<int>
                    {
                        Compatible = number => number.IsAMultipleOf(3) && number.IsAMultipleOf(5),
                        Format = number => EUROFINS
                    }
                }
            };
    }
}
