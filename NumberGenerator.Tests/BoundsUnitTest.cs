﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberGenerator.Library;

namespace NumberGenerator.Tests
{
    [TestClass]
    public class BoundsUnitTest
    {
        /// <summary>
        /// Fails when upper bound is not greater than or equal to lower bound
        /// </summary>
        [TestMethod]
        [TestCategory("Bounds tests")]
        public void ShouldEnsureUpperBoundIsAlwaysGreaterThanOrEqualToLowerBound()
        {
            //arrange
            var bounds = new Bounds();
            bounds.LowerBound = 10;

            //act
            bounds.UpperBound = 9;

            //assert
            Assert.IsTrue(bounds.UpperBound >= bounds.LowerBound, "Upper bound should have been greater than or equal to lower bound");
        }

        /// <summary>
        /// Fails when lower bound is not less than or equal to upper bound
        /// </summary>
        [TestMethod]
        [TestCategory("Bounds tests")]
        public void ShouldEnsureLowerBoundIsAlwaysLessThanOrEqualToUpperBound()
        {
            //arrange
            var bounds = new Bounds();
            bounds.UpperBound = 10;

            //act
            bounds.LowerBound = 11;

            //assert
            Assert.IsTrue(bounds.LowerBound <= bounds.UpperBound, "Lower bound should have been less than or equal to upper bound");
        }
    }
}
