﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberGenerator.Library;
using static NumberGenerator.Library.NumberGenerator;

namespace NumberGenerator.Tests
{
    [TestClass]
    public class NumberGeneratorUnitTest
    {
        /// <summary>
        /// Tests that the number generator produces a list of numbers which greater than or equal to the lower bound
        /// </summary>
        [TestMethod]
        [TestCategory("NumberGenerator tests")]
        public void ShouldGenerateNumbersGreaterThanOrEqualToLowerBound()
        {
            //arrange
            var bounds = new Bounds { LowerBound = 3, UpperBound = 10 };

            //act
            var numbers = GenerateNumbers(bounds);

            //assert
            Assert.IsTrue(bounds.LowerBound == numbers.Min(), "Should have generated numbers which are greater than or equal to the lower bound");
        }

        /// <summary>
        /// Tests that the number generator produces a list of numbers which is less than or equal to the upper bound
        /// </summary>
        [TestMethod]
        [TestCategory("NumberGenerator tests")]
        public void ShouldGenerateNumbersLessThanOrEqualToUpperBound()
        {
            //arrange
            var bounds = new Bounds { UpperBound = 25 };

            //act
            var numbers = GenerateNumbers(bounds);

            //assert
            Assert.IsTrue(bounds.UpperBound == numbers.Max(), "Should have generated numbers which are less than or equal to the upper bound");
        }


        /// <summary>
        /// Passes when the correct number of numbers is generated
        /// </summary>
        [TestMethod]
        [TestCategory("NumberGenerator tests")]
        public void ShouldGenerateTheCorrectNumberOfNumbers()
        {

            //arrange
            var bounds = new Bounds { LowerBound = 1, UpperBound = 10 };

            //act
            var numbers = GenerateNumbers(bounds);

            //assert
            Assert.IsTrue(10 == numbers.Count(), "Should have generated the correct number of numbers");
        }


        /// <summary>
        /// Passes when a lower bound and upper bound of 1 and 100 are used respectively if a user has not specified any lower and upper bounds.
        /// </summary>
        [TestMethod]
        [TestCategory("NumberGenerator tests")]
        public void ShouldUseTheDefaultLowerBoundOf1AndDefaultUpperBoundOf100WhenNoBoundsAreSpecified()
        {
            //arrange
            var bounds = new Bounds();

            //act
            var numbers = GenerateNumbers(bounds);

            //assert
            Assert.IsTrue(1 == numbers.Min() && 100 == numbers.Max(), "Should have used a default lower bound of 1 and  a default upper bound of 100");
        }


        /// <summary>
        /// Passes when a lower bound of 1 is used if a user has not specified a lower bound.
        /// </summary>
        [TestMethod]
        [TestCategory("NumberGenerator tests")]
        public void ShouldUseTheDefaultLowerBoundOf1WhenNoLowerBoundIsSpecified()
        {
            //arrange
            var bounds = new Bounds { UpperBound = 10 };

            //act
            var numbers = GenerateNumbers(bounds);

            //assert
            Assert.IsTrue(1 == numbers.Min(), "Should have used a default lower bound of 1");
        }


        /// <summary>
        /// Passes when an upper bound of 1 is used if a user has not specified an upper bound.
        /// </summary>
        [TestMethod]
        [TestCategory("NumberGenerator tests")]
        public void ShouldUseTheDefaultUpperBoundOf1WhenNoUpperBoundIsSpecified()
        {
            //arrange
            var bounds = new Bounds { LowerBound = 0 };

            //act
            var numbers = GenerateNumbers(bounds);

            //assert
            Assert.IsTrue(100 == numbers.Max(), "Should have used a default upper bound of 100");
        }
    }
}
