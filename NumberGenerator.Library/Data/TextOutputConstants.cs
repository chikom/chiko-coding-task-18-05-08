﻿namespace NumberGenerator.Library.Data
{
    public static class TextOutputConstants
    {
        public const string THREE = "Three";
        public const string FIVE = "Five";
        public const string EUROFINS = "Eurofins";
    }
}
