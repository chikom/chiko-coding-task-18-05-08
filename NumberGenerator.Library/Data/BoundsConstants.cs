﻿namespace NumberGenerator.Library.Data
{
    public class BoundsConstants
    {
        public const int LOWER_BOUND = 1;
        public const int UPPER_BOUND = 100;
    }
}
