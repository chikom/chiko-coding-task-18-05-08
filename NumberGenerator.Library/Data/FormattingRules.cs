﻿using System.Collections.Generic;
using static NumberGenerator.Library.Data.TextOutputConstants;

namespace NumberGenerator.Library.Data
{
    public static class FormattingRules
    {
        public static Dictionary<string, FormattingRule<int>> GetFormattingRules() =>
            new Dictionary<string, FormattingRule<int>>
            {
                {
                    THREE,
                    new FormattingRule<int>
                    {
                        Compatible = number => number.IsAMultipleOf(3) && !number.IsAMultipleOf(5),
                        Format = number => THREE
                    }
                },
                {
                    FIVE,
                    new FormattingRule<int>
                    {
                        Compatible = number => number.IsAMultipleOf(5) && !number.IsAMultipleOf(3),
                        Format = number => FIVE
                    }
                },
                {
                    EUROFINS,
                    new FormattingRule<int>
                    {
                        Compatible = number => number.IsAMultipleOf(3) && number.IsAMultipleOf(5),
                        Format = number => EUROFINS
                    }
                },
            };
    }
}
