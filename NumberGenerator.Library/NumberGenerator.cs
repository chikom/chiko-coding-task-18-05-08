﻿using System.Collections.Generic;
using System.Linq;
using static NumberGenerator.Library.Bounds;

namespace NumberGenerator.Library
{
    public static class NumberGenerator
    {
        /// <summary>
        /// Generates a sequence of numbers which fallws within the given upper and lower bounds.        
        /// </summary>
        /// <param name="bounds"></param>
        /// <returns></returns>
        public static IEnumerable<int> GenerateNumbers(Bounds bounds)
        {
            bounds = bounds ?? new Bounds();
            return Enumerable.Range(bounds.LowerBound, CountNumbersWithinBounds(bounds));
        }
    }
}
