﻿using static NumberGenerator.Library.Data.BoundsConstants;

namespace NumberGenerator.Library
{
    public class Bounds
    {
        private int _LowerBound = LOWER_BOUND;
        private int _UpperBound = UPPER_BOUND;

        public int LowerBound {
            get { return _LowerBound; }
            set { _LowerBound = value <= UpperBound ? value : UpperBound; }            
        }

        public int UpperBound {
            get { return _UpperBound; }
            set { _UpperBound = value >= LowerBound ? value : LowerBound; }
        }

        /// <summary>
        /// Gets the number of numbers within the upper and lower bounds.
        /// </summary>
        /// <param name="bounds"></param>
        /// <returns></returns>
        public static int CountNumbersWithinBounds(Bounds bounds)
        {
            int inclusiveBoundsNumber = 1;
            return bounds.UpperBound - bounds.LowerBound + inclusiveBoundsNumber;
        }
    }
}
