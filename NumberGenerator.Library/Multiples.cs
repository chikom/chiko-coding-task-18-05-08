﻿namespace NumberGenerator.Library
{
    public static class Multiples
    {
        public static bool IsAMultipleOf(this int numberToCheck, int factor) =>
            numberToCheck % factor == 0;        
    }
}
