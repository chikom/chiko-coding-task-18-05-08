﻿using System;

namespace NumberGenerator.Library
{
    public class FormattingRule <T>
    {        
        public Func<T, bool> Compatible { get; set; }
        public Func<T, string> Format { get; set; }        
    }
}
