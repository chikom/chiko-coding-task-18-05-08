﻿using System.Collections.Generic;
using System.Linq;
using static NumberGenerator.Library.Data.FormattingRules;

namespace NumberGenerator.Library
{
    public static class NumberFormatter
    {
        /// <summary>
        /// Formats an based on a given formatting rule
        /// </summary>
        /// <param name="item"></param>
        /// <param name="formattingRules"></param>
        /// <returns></returns>       
        public static string FormatConditionally<T>(
            T item, Dictionary<string, FormattingRule<T>> formattingRules)
        {
            foreach (var formattingRule in formattingRules)
            {
                if(formattingRule.Value == null)
                {
                    continue;
                }

                if (formattingRule.Value.Compatible(item))
                {
                    return formattingRule.Value.Format(item);
                }
            }

            return item.ToString();
        }  
        
        /// <summary>
        /// Formats a list of numbers
        /// </summary>
        /// <param name="numbers"></param>
        /// <param name="formattingRules"></param>
        /// <returns></returns>
        public static IEnumerable<string> FormatNumbers(
            IEnumerable<int> numbers, Dictionary<string, FormattingRule<int>> formattingRules) =>
            numbers.Select(n => FormatConditionally(n, formattingRules));        
    }
}
